[![Build status](https://psdstewards.visualstudio.com/PSD/_apis/build/status/proscrumdev.battleship-java-CI)](https://psdstewards.visualstudio.com/PSD/_build/latest?definitionId=15)

# Battleship Java

A simple game of Battleship, written in Java.

# Getting started

This project requires a Java JDK 8 or higher. To prepare to work with it, pick one of these
options:

## Run locally

Run battleship with Gradle

```bash
./gradlew run
```

Execute tests with Gradle

```bash
./gradlew test
```

## Docker

If you don't want to install anything Java-related on your system, you can
run the game inside Docker instead.

### Run a Docker Container from the Image

```bash
docker run -it -v ${PWD}:/battleship -w /battleship openjdk:15 bash
```

This will run a Docker container with your battleship case study mounted into it. The container will run in interactive mode and you can execute Gradle commands from the shell (see examples below).

# Launching the game

```bash
./gradlew run
```

# Running the Tests

```
./gradlew test
```

# Game interface

```
For your convinience messages during the game has diffrent colour:
 - green - it's your input
 - red - you hit an enemy ship
 - blue - you attacked the ship by
 - yellow - tips for your actions
 - gray - all other messages
 
 
 
Before each step, you are told the possible options.

All ships are randomly assigned each time the game starts
```
