package org.scrum.psd.battleship.ascii;

import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import org.scrum.psd.battleship.controller.dto.Letter;
import org.scrum.psd.battleship.controller.dto.Position;
import org.scrum.psd.battleship.controller.dto.Ship;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Execution(ExecutionMode.SAME_THREAD)
public class MainTest {

    @Before
    public void before() {

    }

    @Test
    public void testParsePosition() {
        Position actual = Main.parsePosition("A1");
        Position expected = new Position(Letter.A, 1);
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void testParsePosition2() {
        //given
        Position expected = new Position(Letter.B, 1);
        //when
        Position actual = Main.parsePosition("B1");
        //then
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void testShipsAreNotIntersected() {
        Main.InitializeEnemyFleet();
        final List<Ship> enemyFleet = Main.getEnemyFleet();
        Set<Position> positions = new HashSet<>();
        for (Ship ship : enemyFleet) {
            for (Position position : ship.getPositions()) {
                Assert.assertTrue(String.format("ship - %s, position - %s", ship.toString(), position.toString()), positions.add(position));
            }
        }
    }

    @Test
    public void testShipsAreNotOutOfBoard() {
        Main.InitializeEnemyFleet();
        final List<Ship> enemyFleet = Main.getEnemyFleet();
        for (Ship ship : enemyFleet) {
            for (Position position : ship.getPositions()) {
                Assert.assertTrue(position.getRow() >= 0 && position.getRow() < 10);
            }
        }
    }

    @Test
    public void testShipsSizeCorrect() {
        Main.InitializeEnemyFleet();
        final List<Ship> enemyFleet = Main.getEnemyFleet();
        Assert.assertTrue(enemyFleet.get(0).getSize() == 5);
        Assert.assertTrue(enemyFleet.get(1).getSize() == 4);
        Assert.assertTrue(enemyFleet.get(2).getSize() == 3);
        Assert.assertTrue(enemyFleet.get(3).getSize() == 3);
        Assert.assertTrue(enemyFleet.get(4).getSize() == 2);
    }

    @Test
    public void testShipsDirectionCorrect() {
        Main.InitializeEnemyFleet();
        final List<Ship> enemyFleet = Main.getEnemyFleet();
        for (Ship ship : enemyFleet) {
            int directionVertical = 1;
            int directionHorizontal = 1;
            Position prevPosition = null;
            for (Position position : ship.getPositions()) {
                if(prevPosition != null) {
                    final String message = String.format("ship - %s, position - %s, prev position - %s", ship.toString(), position.toString(),
                            prevPosition.toString());
                    if(prevPosition.getRow() == position.getRow()) {
                        directionHorizontal++;
                        Assert.assertEquals(message,
                                Math.abs(prevPosition.getColumn().ordinal() - position.getColumn().ordinal()), 1);
                    } else if(prevPosition.getColumn() == position.getColumn()) {
                        directionVertical++;
                        Assert.assertEquals(message,
                                Math.abs(prevPosition.getRow() - position.getRow()), 1);
                    }
                }
                prevPosition = position;
            }
            Assert.assertEquals(String.format("ship - %s", ship.toString()), ship.getSize(), Math.max(directionHorizontal, directionVertical));
        }
    }

}
