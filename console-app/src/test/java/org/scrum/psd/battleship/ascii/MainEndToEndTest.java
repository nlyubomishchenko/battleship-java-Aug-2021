package org.scrum.psd.battleship.ascii;

import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.junit.contrib.java.lang.system.TextFromStandardInputStream;
import org.apache.commons.io.IOUtils;
import org.scrum.psd.battleship.controller.dto.Letter;
import org.scrum.psd.battleship.controller.dto.Position;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.NoSuchElementException;
import java.util.Objects;

import static org.junit.contrib.java.lang.system.TextFromStandardInputStream.emptyStandardInputStream;

public class MainEndToEndTest {
    @ClassRule
    public static final SystemOutRule systemOutRule = new SystemOutRule().enableLog();
    @ClassRule
    public static final TextFromStandardInputStream gameInput = emptyStandardInputStream();

    @Before
    public void beforeEach() {
        systemOutRule.clearLog();
    }

    @Test
    public void testPlayGameShotHits() throws IOException {
        try {
            Main.setGetRandomPosition(() -> new Position(Letter.H, 6));
            Main.setGetFieldNumber(() -> 1);
            gameInput.provideLines("a1", "a2", "a3", "a4", "a5", "b1", "b2", "b3", "b4", "c1", "c2", "c3", "d1", "d2", "d3", "e1", "e2", "b4");
            Main.main(new String[]{});
        } catch (NoSuchElementException e) {
            String actual = IOUtils.toString(Objects.requireNonNull(this.getClass().getResourceAsStream("/console_out/hit.txt")), StandardCharsets.UTF_8);
            Assert.assertEquals(systemOutRule.getLog(), actual);
        }
    }

    @Test
    public void testIntersectionInputPosition() throws IOException {
        try {
            Main.setGetRandomPosition(() -> new Position(Letter.H, 6));
            Main.setGetFieldNumber(() -> 1);
            gameInput.provideLines("a1", "a1");
            Main.main(new String[]{});
        } catch (NoSuchElementException e) {
            String actual = IOUtils.toString(Objects.requireNonNull(this.getClass().getResourceAsStream("/console_out/intersect.txt")), StandardCharsets.UTF_8);
            Assert.assertEquals(systemOutRule.getLog(), actual);
        }
    }

    @Test
    public void testPlayGameShotMisses() throws IOException {
        try {
            Main.setGetRandomPosition(() -> new Position(Letter.H, 6));
            Main.setGetFieldNumber(() -> 1);
            gameInput.provideLines("a1", "a2", "a3", "a4", "a5", "b1", "b2", "b3", "b4", "c1", "c2", "c3", "d1", "d2", "d3", "e1", "e2", "e4");
            Main.main(new String[]{});
        } catch (NoSuchElementException e) {
            String actual = IOUtils.toString(Objects.requireNonNull(this.getClass().getResourceAsStream("/console_out/miss.txt")), StandardCharsets.UTF_8);
            Assert.assertEquals(systemOutRule.getLog(), actual);
        }
    }
}
